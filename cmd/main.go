package main

import (
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/pgt502/messages/handlers"
	"bitbucket.org/pgt502/messages/store"
	"bitbucket.org/pgt502/messages/types"
	"github.com/gin-gonic/gin"
)

func main() {

	path, ok := os.LookupEnv("CONFIG_PATH")
	if !ok {
		log.Fatal("CONFIG_PATH environment variable needs to be set")
	}
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("failed to read config file, err: %v", err)
	}
	var cfg types.Configuration
	cfg.Deserialise(buf)

	router := gin.Default()

	var db store.MessageStore
	if cfg.UseGcpDataStore {
		db, err = store.NewGCPMessageStore(cfg.ProjectID)
	} else {
		db, err = store.NewMemoryMessageStore()
	}
	if err != nil {
		log.Fatal(err)
	}
	msgHandler := handlers.NewMsgHandler(db)
	msgHandler.AddRoutes(router)

	digestHandler := handlers.NewDigestHandler(db)
	digestHandler.AddRoutes(router)

	err = router.Run(cfg.Address)
	log.Fatal(err)
}
