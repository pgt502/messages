package store

import (
	"fmt"

	"bitbucket.org/pgt502/messages/types"
)

type memoryStore struct {
	db map[string]*types.Message
}

func NewMemoryMessageStore() (MessageStore, error) {
	return &memoryStore{
		db: make(map[string]*types.Message),
	}, nil
}

func (s *memoryStore) Store(msg *types.Message) (err error) {
	if msg.Digest == "" {
		err = fmt.Errorf("memoryStore: message digest cannot be empty")
		return
	}
	s.db[msg.Digest] = msg
	return
}

func (s *memoryStore) Get(id string) (msg *types.Message, err error) {
	if id == "" {
		err = fmt.Errorf("memoryStore: message id cannot be empty")
		return
	}
	msg, exists := s.db[id]
	if !exists {
		err = fmt.Errorf("memoryStore: message for key: [%s] does not exist", id)
	}
	return
}
