package store

import "bitbucket.org/pgt502/messages/types"

type MessagePersister interface {
	Store(*types.Message) error
}

type MessageFetcher interface {
	Get(string) (*types.Message, error)
}

type MessageStore interface {
	MessagePersister
	MessageFetcher
}
