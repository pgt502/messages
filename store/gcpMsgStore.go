package store

import (
	"context"
	"fmt"
	"log"

	"bitbucket.org/pgt502/messages/types"
	"cloud.google.com/go/datastore"
)

type gcpDataStore struct {
	client *datastore.Client
}

// NewGCPMessageStore creates an instance of GCP message store
func NewGCPMessageStore(projectID string) (MessageStore, error) {
	ctx := context.Background()
	// Verify that we can communicate and authenticate with the datastore service.
	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		return nil, fmt.Errorf("datastoredb: could not connect: %v", err)
	}
	t, err := client.NewTransaction(ctx)
	if err != nil {
		return nil, fmt.Errorf("datastoredb: could not connect: %v", err)
	}
	if err := t.Rollback(); err != nil {
		return nil, fmt.Errorf("datastoredb: could not connect: %v", err)
	}
	return &gcpDataStore{
		client: client,
	}, nil
}

func (s *gcpDataStore) Store(msg *types.Message) (err error) {
	if msg.Digest == "" {
		err = fmt.Errorf("datastoredb: message digest cannot be empty")
		return
	}
	key := s.createKey(msg.Digest)
	log.Printf("storing data with key: [%s]", key.String())
	_, err = s.client.Put(context.Background(), key, msg)
	if err != nil {
		err = fmt.Errorf("datastoredb: could not put message: %v", err)
	}
	return
}

func (s *gcpDataStore) createKey(id string) *datastore.Key {
	return datastore.NameKey("digest", id, nil)
}

func (s *gcpDataStore) Get(id string) (ret *types.Message, err error) {
	key := s.createKey(id)
	var msg types.Message
	err = s.client.Get(context.Background(), key, &msg)
	if err != nil {
		err = fmt.Errorf("datastoredb: could not get message: [%s], key: [%s], err: %v", id, key.String(), err)
	}
	ret = &msg
	return
}
