# Messages API

The messages API has 2 endpoints:

1. POST /messages
2. GET /messages/:digest

### POST /messages
It expects the request body to be in json format and have a "message" key, for example:
```
{
	"message" : "foo"
}
```

The response will look as follows:
```
{
	"digest" : "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae"
}
```

### GET /messages/:digest
For example if digest is "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae" the url will be:
`/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae`

The response for an existing digest will look as follows:
```
{
	"message" : "foo"
}
```

Whereas a response for non-existing digest will return an error:
```
{
	"error" : "Message not found"
}
```
---
## Building and running
### Using Go (requires Go environment)
1. Clone the repo into GOPATH:
`git clone https://pgt502@bitbucket.org/pgt502/messages.git`
2. Go to the root of the project: `cd messages`
3. Go to `cmd` directory: `cd cmd`
4. Build the executable: `go build -o api`
5. Export environment variable which specifies location of the config file (if config.json is in the current directory):
`export CONFIG_PATH=$(pwd)/config.json`
6. Run the executable: `./api`

### Using Docker and Docker Compose
1. Clone the repo:
`git clone https://pgt502@bitbucket.org/pgt502/messages.git`
2. Go to the root of the project: `cd messages`
3. Use makefile to build and run: `make`
---
## Running Tests (requires Go environment)
1. Clone the repo into GOPATH (if not already done):
`git clone https://pgt502@bitbucket.org/pgt502/messages.git`
2. Go to the root of the project: `cd messages`
3. Go to `handlers` directory: `cd handlers`
4. Run the tests: `go test`
