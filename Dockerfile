FROM golang 
RUN mkdir /build
ADD . /build
WORKDIR /build
ENTRYPOINT [ "./docker-build.sh" ]