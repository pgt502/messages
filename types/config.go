package types

import (
	"encoding/json"
)

type Configuration struct {
	ProjectID       string
	UseGcpDataStore bool
	Address         string
}

func (c *Configuration) Deserialise(bytes []byte) error {
	cfg := &Configuration{}
	err := json.Unmarshal(bytes, cfg)
	if err != nil {
		return err
	}
	*c = *cfg
	return err
}
