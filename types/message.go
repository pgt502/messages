package types

type Message struct {
	Message string `json:"message"`
	Digest  string `json:"digest"`
}

func NewMessage(msg, dig string) *Message {
	return &Message{
		Message: msg,
		Digest:  dig,
	}
}
