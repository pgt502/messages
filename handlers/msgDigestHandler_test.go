package handlers_test

import (
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "bitbucket.org/pgt502/messages/handlers"
	"bitbucket.org/pgt502/messages/store"
	"bitbucket.org/pgt502/messages/types"
)

var _ = Describe("MsgDigestHandler", func() {
	Describe("handling requests", func() {
		var (
			handler  Handler
			db       store.MessageStore
			resp     *httptest.ResponseRecorder
			endpoint Endpoints
			router   Server
		)
		BeforeEach(func() {
			var err error
			db, err = store.NewMemoryMessageStore()
			Expect(err).ToNot(HaveOccurred(), "instantiating msg store")
			handler = NewDigestHandler(db)
			router = gin.Default()
			handler.AddRoutes(router)
		})
		Context("when message is valid", func() {
			Context("when digest exists in the store", func() {
				BeforeEach(func() {
					msg := &types.Message{
						Message: "foo",
						Digest:  "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae",
					}
					err := db.Store(msg)
					Expect(err).NotTo(HaveOccurred())
				})
				It("should return correct code and a hash of the message", func() {
					endpoint = Endpoints{
						Router:  router,
						Headers: map[string]string{},
					}
					resp = endpoint.GET("/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae")
					Expect(resp.Code).To(Equal(200), "response code")
					Expect(resp.Body).ToNot(BeNil(), "response body")
					Expect(resp.Body.String()).To(MatchJSON(`{"message":"foo"}`), "response body")
				})
			})
			Context("when digest doesnt exist in the store", func() {
				It("should return code 404 and error message", func() {
					endpoint = Endpoints{
						Router:  router,
						Headers: map[string]string{},
					}
					resp = endpoint.GET("/messages/nonexistentdigest")
					Expect(resp.Code).To(Equal(404), "response code")
					Expect(resp.Body).ToNot(BeNil(), "response body")
					Expect(resp.Body.String()).To(MatchJSON(`{"error":"Message not found"}`), "response body")
				})
			})
		})
		// TODO: test different messages
		// TODO: test different headers
		// TODO: test different http verbs
	})
})
