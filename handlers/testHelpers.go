package handlers

import (
	"io"
	"net/http/httptest"
)

type Endpoints struct {
	Router  Server
	Headers map[string]string
}

func (u *Endpoints) POST(url string, body io.Reader) (resp *httptest.ResponseRecorder) {
	req := httptest.NewRequest("POST", url, body)
	for key, value := range u.Headers {
		req.Header.Add(key, value)
	}
	resp = httptest.NewRecorder()
	u.Router.ServeHTTP(resp, req)
	return resp
}

func (u *Endpoints) GET(url string) (resp *httptest.ResponseRecorder) {
	req := httptest.NewRequest("GET", url, nil)
	for key, value := range u.Headers {
		req.Header.Add(key, value)
	}
	resp = httptest.NewRecorder()
	u.Router.ServeHTTP(resp, req)
	return resp
}
