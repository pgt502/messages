package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Handler interface {
	Handle(c *gin.Context)
	AddRoutes(r Server)
}

type Server interface {
	ServeHTTP(http.ResponseWriter, *http.Request)
	POST(relativePath string, handlers ...gin.HandlerFunc) gin.IRoutes
	GET(relativePath string, handlers ...gin.HandlerFunc) gin.IRoutes
}
