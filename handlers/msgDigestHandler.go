package handlers

import (
	"log"
	"net/http"

	"bitbucket.org/pgt502/messages/store"
	"github.com/gin-gonic/gin"
)

type msgDigestHandler struct {
	msgStore store.MessageStore
}

// NewDigestHandler creates a new instance of Message Digest Handler
func NewDigestHandler(msgStore store.MessageStore) Handler {
	return &msgDigestHandler{
		msgStore: msgStore,
	}
}

func (h *msgDigestHandler) AddRoutes(r Server) {
	r.GET("/messages/:digest", h.Handle)
}

func (h *msgDigestHandler) Handle(c *gin.Context) {
	digest := c.Param("digest")
	msg, err := h.msgStore.Get(digest)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusNotFound, gin.H{"error": "Message not found"})
		return
	}
	c.JSON(200, gin.H{
		"message": msg.Message,
	})
}
