package handlers

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/pgt502/messages/store"
	"bitbucket.org/pgt502/messages/types"
	"github.com/gin-gonic/gin"
)

type msgHashHandler struct {
	msgStore store.MessageStore
}

// NewMsgHandler creates a new instance of Message Hash Handler
func NewMsgHandler(msgStore store.MessageStore) Handler {
	return &msgHashHandler{
		msgStore: msgStore,
	}
}

func (h *msgHashHandler) AddRoutes(r Server) {
	r.POST("/messages", h.Handle)
}

func (h *msgHashHandler) Handle(c *gin.Context) {
	var req types.Message
	if err := c.ShouldBindJSON(&req); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if req.Message == "" {
		err := fmt.Errorf("wrong message format")
		raw, _ := c.GetRawData()
		log.Printf("%s, %+v", err, raw)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	hasher := sha256.New()
	hasher.Write([]byte(req.Message))
	digest := hasher.Sum(nil)

	req.Digest = hex.EncodeToString(digest)
	if err := h.msgStore.Store(&req); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(200, gin.H{
		"digest": req.Digest,
	})
}
