package handlers_test

import (
	"bytes"
	"net/http/httptest"

	"bitbucket.org/pgt502/messages/store"
	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "bitbucket.org/pgt502/messages/handlers"
)

var _ = Describe("MsgHashHandler", func() {
	Describe("handling requests", func() {
		var (
			handler  Handler
			db       store.MessageStore
			resp     *httptest.ResponseRecorder
			endpoint Endpoints
			router   Server
		)
		BeforeEach(func() {
			var err error
			db, err = store.NewMemoryMessageStore()
			Expect(err).ToNot(HaveOccurred(), "instantiating msg store")
			handler = NewMsgHandler(db)
			router = gin.Default()
			handler.AddRoutes(router)
		})
		Context("when message is valid", func() {
			It("should return correct code and a hash of the message", func() {
				endpoint = Endpoints{
					Router: router,
					Headers: map[string]string{
						"Content-Type": "application/json",
					},
				}
				body := bytes.NewReader([]byte(`{ "message" : "foo" }`))
				resp = endpoint.POST("/messages", body)
				Expect(resp.Code).To(Equal(200), "response code")
				Expect(resp.Body).ToNot(BeNil(), "response body")
				Expect(resp.Body.String()).To(MatchJSON(`{"digest":"2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae"}`), "response body")
			})
		})
		Context("when wrong request body is provided", func() {
			It("should return an error", func() {
				endpoint = Endpoints{
					Router: router,
					Headers: map[string]string{
						"Content-Type": "application/json",
					},
				}
				body := bytes.NewReader([]byte(`{}`))
				resp = endpoint.POST("/messages", body)
				Expect(resp.Code).To(Equal(400), "response code")
				Expect(resp.Body).ToNot(BeNil(), "response body")
				Expect(resp.Body.String()).To(MatchJSON(`{"error":"wrong message format"}`), "response body")
			})
		})
		// TODO: test different headers
		// TODO: test different http verbs
	})
})
