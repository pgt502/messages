run: build	
	docker-compose up -d 

build: 
	
	docker-compose -f docker-compose.build.yml up --build --remove-orphans
	docker-compose -f docker-compose.build.yml build
	