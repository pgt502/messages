#!/bin/bash

echo "in build now"
DEV="/go/src/bitbucket.org/pgt502/messages"
mkdir -p ${DEV}
cd ${DEV}
cp -r /build/* .

cd cmd
go build -o api
mv api /out/api/api
cp config.json /out/api/config.json
